<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $mailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(object $mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Build the message. 
     * @return $this
     */
    public function build()
    {
        $email = $this->from($this->mailData->from)->subject($this->mailData->subject)->view('emails.default')->with([
            'mail_data' => $this->mailData,
        ]);

        foreach($this->mailData->files as $filePath){
            $email->attach('storage/' . $filePath); 
        }

        return $email;
    }
}
