<?php

namespace App\Http\Controllers;

use App\Email;
use App\EmailFile;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;

class MailController extends Controller
{

    public function __construct()
    {

    }

    public function send(Request $request)
    {
        $attributes = request()->validate([
            'from' => ['bail', 'required'],
            'to' => ['required'],
            'subject' => ['required'],
        ]);

        $attributes['textContent'] = $request->get("textContent");
        $attributes['htmlContent'] = $request->get("htmlContent");
        $attributes['status'] = 'Posted';
        $email = Email::create($attributes);

        $count = (int) $request->get("fileCount");
        $emailFiles = [];

        for ($i = 0; $i < $count; $i++) {
            $attachment = $request->file('file' . $i);
            $name = $attachment->store('attachments', 'public');
            EmailFile::create(
                [
                    'email_id' => $email->id,
                    'file' => $name,
                ]);

            $emailFiles[] = $name;
        }

        $mailData = new \stdClass();
        $mailData->from = $request->get('from');
        $mailData->subject = $request->get('subject');
        $mailData->textContent = $request->get('textContent');
        $mailData->htmlContent = $request->get('htmlContent');
        $mailData->files = $emailFiles;

        try {
            Mail::to($request->get('to'))
                ->queue(new SendMail($mailData));

            Email::where("id", $email->id)->update(['status' => 'Sent']);

            return response()->json([
                'message' => 'Email Sent',
            ]);
        } catch (\Exception $e) {
            Email::where("id", $email->id)->update(['status' => 'Failed']);

            return response()->json([
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function getEmails(Request $request)
    {
        $emails = DB::table('emails');
        $count = $emails->count();
        $emails = $emails->latest()->limit($request->get('limit'))->skip($request->get('offset'))->get();

        foreach ($emails as $email) {
            $email->files = DB::table('email_files')->where('email_id', '=', $email->id)->get();
        }

        return response()->json([
            'emails' => $emails,
            'total' => $count,
        ]);
    }

    public function searchEmails(Request $request)
    {
        if ($request->get('search') != "") {
            $q = $request->get('search');
            $emails = DB::table('emails')->where(function ($query) use ($q) {
                $query->where('from', 'LIKE', '%' . $q . '%')
                    ->orWhere('to', 'LIKE', '%' . $q . '%')
                    ->orWhere('subject', 'LIKE', '%' . $q . '%');
            });

            $count = $emails->count();
            $emails = $emails->latest()->limit($request->get('limit'))->skip($request->get('offset'))->get();
        }

        return response()->json([
            'emails' => $emails,
            'total' => $count,
        ]);
    }
}
